-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jun 29, 2015 at 11:03 AM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `iqra`
--

-- --------------------------------------------------------

--
-- Table structure for table `scrapper`
--

CREATE TABLE IF NOT EXISTS `scrapper` (
`scrap_id` int(255) NOT NULL,
  `allowed_words` varchar(255) NOT NULL,
  `banned_words` varchar(255) NOT NULL,
  `allow_filter` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `scrapper`
--

INSERT INTO `scrapper` (`scrap_id`, `allowed_words`, `banned_words`, `allow_filter`) VALUES
(1, 'academics', 'school', 'programs'),
(2, '2015', 'prospectus', 'undergrad'),
(3, '2016', 'download', 'study'),
(4, 'prospective', 'entrance', 'ug'),
(5, 'ug', 'Events', 'Undergraduate'),
(6, 'Browse A-Z of courses', 'event', 'subjects'),
(7, 'under-graduate', 'Feedback', 'undergraduate'),
(8, 'undergraduate', 'Opendays', 'courses'),
(9, 'Undergraduate', 'openday', 'course'),
(10, 'course', 'Open days', 'degree'),
(11, 'degree', 'sign in', 'programmes'),
(12, 'degrees', 'Signin', 'programme'),
(13, 'Degree', 'portal', 'Degree'),
(14, 'programmes', 'taught', '1'),
(15, 'Course', 'research', '2'),
(16, 'courses', 'news', '3'),
(17, 'Courses', 'thrive', '4'),
(18, 'study', 'lang=cym', '5'),
(19, 'Studying', 'caerdydd', '6'),
(20, 'studying', 'Array', '7'),
(21, 'undergrad', 'mailto', '8'),
(22, 'ugrad', 'why', '9'),
(23, 'u-grad', 'text', '0'),
(24, 'Academics', 'extrenal', ''),
(25, 'Ug', 'Freequently', ''),
(26, 'fields', '%', ''),
(29, 'programme', 'international', ''),
(30, 'programe', 'feedback', ''),
(33, 'Programe', 'contact', ''),
(34, 'Degrees', 'about', ''),
(35, '', 'Read more', ''),
(39, '', '+44', '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `scrapper`
--
ALTER TABLE `scrapper`
 ADD PRIMARY KEY (`scrap_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `scrapper`
--
ALTER TABLE `scrapper`
MODIFY `scrap_id` int(255) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=41;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
