<?PHP
	ini_set("memory_limit","2048M");
	error_reporting(0);
	include ('connection.php');
	$pagination=array();
	$filter=array();
	$count=0;
	
	
	function link_extract($a){
		global $conn;
		$result = mysqli_query($conn, "SELECT  `word` FROM `scrapper` where type='0'");
		
		while ($row = mysqli_fetch_assoc($result)) {
		
			$filter[] = $row['word'];	
		}
		
		
		foreach ($a as $url) {
				//echo $url;			
				// $curl = curl_init();
				// curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
				// curl_setopt($curl, CURLOPT_URL, $url);
				// curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
				// curl_setopt($curl, CURLOPT_HEADER, false);
				// $input = curl_exec($curl);
				// curl_close($curl); 
			
				// if(curl_exec($curl) === false)
				// {
				    // echo 'Curl error: ' . curl_error($curl);
				// }
				// else
				// {
				    // echo 'Operation completed without any errors';
				// }
				
			//echo $url."<br>";
			$input = file_get_contents($url);
			// if($url=="http://www2.mmu.ac.uk/study/undergraduate/courses/atoz/index.php?year=2016&letter=b"){
			// exit;	
			// }
			
			//print_r($input);
			$regexp = "<a\s[^>]*href=(\"??)([^\" >]*?)\\1[^>]*>(.*)<\/a>";
			
			if (preg_match_all("/$regexp/siU", $input, $matches)) {
			
				foreach ($matches[0] as $a) {
					foreach ($filter as $b) {
						$pos = strpos($a, $b);
											
						if ($pos !== FALSE) {
							//echo $b.$a."aaaaaaa";
							//echo"<br>";	
							//echo $pos;
							$restrict[] = $a;
							//print_r($restrict);
							//echo $a;
								}
						
							else{
								$allow[]=$a;
								//echo $a;
								//echo"<\n>";
							}
							}
				
						}
						
				}
		}//end of function result
		
	
	RETURN array($allow,$restrict);
	}
	

	
	function result($a,$uni_url,$id) {
	
		global $conn;
		global $count; 
		$count=0;
		$a = array_unique($a);
		// print_r($a);
		// exit;
		
		
		array_unique($a);
		list($allow,$restrict)=link_extract($a);
		
		$latest = array_diff($allow, $restrict);
		$latest = array_unique($latest);
		// print_r($latest);
		// exit;
		
		?>
		
		
			<button type="button" id="hide_programs">Hide All</button> 
		
		    <table style="">
  
			<form method="post" action="check.php">
			<?php
			foreach ($latest as $a) {
								
				$campus = mysqli_query($conn, "SELECT campus_name,campus_id FROM `campus` WHERE uni_id='$id'");
				$currency = mysqli_query($conn, " SELECT  `currency_name` FROM `program_currency` ");
				$field = mysqli_query($conn, "SELECT  `field_name` FROM `field` WHERE uni_id='$id'");
				
				list($prog_href,$prog_name)=getUrlLinkText($a);
				$prog_href=trim($prog_href, '"');
				$prog_href=trim($prog_href, "'");
				
				$dbl= substr($prog_href, 0,2);
				if($dbl=="//"){
					$prog_href=substr_replace($prog_href,"",0,2);
				}
				
				$rest = substr($prog_href, 0,1);
				
				if($rest=="/"){
					$prog_href=$uni_url.$prog_href;
				}
				
				if(strlen($prog_name)==1){
					
					$pagination[]=$prog_href;
				
				}
				
				if(strlen($prog_name)>4){
				
					?>
					<tr>
						
						<td >	
							<input type="checkbox" name="scrap[<?php echo ++$count; ?>]" value="<?php echo $prog_href . "prog:" . strip_tags($prog_name); ?>">
						</td>
						
						<td >	
							<?php echo strip_tags($prog_name)?>
						</td>
						
						<td  >
							
							 <select class="campus" data-code = "<?php echo $camp_count = $count++; ?>" id="campus_<?php echo $camp_count; ?>" name="camp[<?php echo $camp_count; ?>]">
							  <option >Campus</option>	
							  
							  <?php
							  while ($row = mysqli_fetch_assoc($campus)) {
							  ?>
							  <option code="<?php echo $count++;?>" value="<?php echo $row['campus_id']; ?>">	<?php echo $row['campus_name']; ?></option>	
							  <?php
							  }
							  ?>
							</select> 
						</td>
						
						<td  >
							
						<select id="department_<?php echo $camp_count; ?>" name="dept[<?php echo $camp_count; ?>]">
						<option>Department</option>	
						</select> 
						</td>
						<td  >
							 <select id="currency" name="curr[<?php echo $camp_count; ?>]">
							 <?php
							  while ($row = mysqli_fetch_assoc($currency)) {
							  ?>
							  <option value="<?php echo $row['currency_name']; ?>">	<?php echo $row['currency_name']; ?></option>	
							  <?php
							  }
							  ?>
							</select> 
						</td>
						<td  >
							 <select id="Field" name="field[<?php echo $camp_count; ?>]">
							  <option >Field</option>
							  <?php
							   while ($row = mysqli_fetch_assoc($field)) {
							  ?>
							  <option value="<?php echo $row['field_name']; ?>">	<?php echo $row['field_name']; ?></option>	
							  <?php
							  }
							  ?>
							</select> 
						</td>			
					</tr>
					<input type="hidden" name="id" value="<?php echo $id?>">
					
					<?php
					
					
				}
			}
			?>
			</table>
			<input type="submit" value="Submit">
			</form>
			
		<?php
	
	}
	
	function getUrlLinkText($url) {
		
		PREG_MATCH('/<a\s[^>]*href=(\"??)([^\" >]*?)\\1[^>]*>(.*)<\/a>/siU', $url, $matches);
		
		//*** return the match ***/
		
		RETURN array($matches[2],$matches[3]);
	
	}
?>

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>

<script type="text/javascript">
	
	$(document).ready(function(){
		
		
		$('.campus').on('change', function() {
			
			var elem_num = $(this).data("code");
			//alert(elem_num);
	 		var campus_selection = $('#campus_'+elem_num+' option:selected').attr('value');
	 		//alert(campus_selection);
	 		var url="departments_fetch.php";
	 			$.ajax({
		 		url: url,
				data: "campus_id="+campus_selection,
				type: "POST",
				cache: false,
				success: function(result)
				{
					$("#department_"+elem_num).html(result);
				}
			});
		});
		//hide all non checked boxes
		$('#hide_programs').click(function(){
			
			$("input:checkbox").not(":checked").parent().siblings().hide();
			$("input:checkbox").not(":checked").hide();
		});
		
		
				
	});
	
	$(window).load(function(event)
	{
		$('#campus').trigger("change");
	});
	







		
</script>
