<?php

	function pageLinks($html, $current_url = "") {
		preg_match_all("/\<a.+?href=(\"|')(?!javascript:|#)(.+?)(\"|')/i", $html, $matches);
		$links = array();
		if (isset($matches[2])) {
			$links = $matches[2];
			//print_r($matches);
		}
	
		if (count($links) > 0 && strlen($current_url) > 0) {
			$pathi = pathinfo($current_url);
			$dir = $pathi["dirname"];
			$base = parse_url($current_url);
			$split_path = explode("/", $dir);
			$url = "";
	
			foreach ($links as $k => $link) {
				if (preg_match("/^\.\./", $link)) {//if directory ".." found in link
					$total = substr_count($link, "../");
					for ($i = 0; $i < $total; $i++) {
						array_pop($split_path);
					}
					// remove last ../ from url
					$url = implode("/", $split_path) . "/" . str_replace("../", "", $link);
					
					
				} 	// check for // in url
					elseif (preg_match("/^\/\//", $link)) {
						$url = $base["scheme"] . ":" . $link;
				}	
					//check for / or http// in url
					 elseif (preg_match("/^\/|^.\//", $link)) {
						$url = $base["scheme"] . "://" . $base["host"] . $link;
				} 
					elseif (preg_match("/^[a-zA-Z0-9]/", $link)) {// general check
						if (preg_match("/^http/", $link)) {
							$url = $link;
						} 
							else {// if link contain only words or numbers
								$url = $dir . "/" . $link;
						}
				}
				$links[$k] = $url;
			}
		}
		return $links;
	}
?>