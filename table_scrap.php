<?php
include('simple_html_dom.php');
$str = '<table>
<caption style="outline: medium none;">Fees for undergraduates for the 2016/17 academic year</caption> <tbody> <tr><th scope="col">Subject Area</th><th scope="col">Home/EU full-time</th><th scope="col">Home/EU part-time</th><th style="outline: medium none;" scope="col">International full-time</th><th style="outline: medium none;" scope="col">Channel Islands and Isle of Man</th></tr> </tbody> <tbody style="outline: medium none;"> <tr> <td scope="row">Arts courses</td> <td scope="row">£9,000</td> <td scope="row">£4,500</td> <td style="outline: medium none;" scope="row">£15,600*</td> <td style="outline: medium none;" scope="row">£9,000**</td> </tr> <tr> <td scope="row">Science courses</td> <td scope="row">£9,000</td> <td scope="row">£4,500</td> <td style="outline: medium none;" scope="row">£18,800</td> <td style="outline: medium none;" scope="row">£10,500**</td> </tr> <tr> <td scope="row">Clinical courses</td> <td scope="row">£9,000</td> <td scope="row">£4,500</td> <td scope="row">£35,000***</td> <td scope="row">£19,000**</td> </tr> </tbody> 


</table>';

$html = str_get_html($str);

 $tds = $html->find('table',0)->find('td');
 $num = null;
 foreach($tds as $td){

     if($td->plaintext == 'Arts courses'){

        $next_td = $td->next_sibling()->next_sibling();
        $num = $next_td->plaintext ;    
        break; 
     }
 }

 echo($num);
 
?>