<head>
	
<style type="text/css">
        .txtStyle
        {
            resize: none;
            width: 97.1%;
            overflow: auto;
            padding: 5px;
            border: 1px dotted #B6B7BC;
            height: 100px;
        }
        .copybtn
        {
            display: inline-block;
            outline: none;
            padding: 5px;
            text-decoration: none;
            text-align: center;
            width: 85%;
            vertical-align: middle;
            line-height: 30px;
            color: #000;
            background-color: #e2e2e2;
            border: 1px solid #A9A9A9;
        }
        .copybtn.zeroclipboard-is-hover
        {
            color: #000;
            background-color: #B6B7BC;
            border-color: #000;
        }
        .copybtn.zeroclipboard-is-active
        {
            color: #000;
            background-color: #B6B7BC;
            border-color: #000;
        }
    </style>
    <script type="text/javascript" src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
<script type="text/javascript" src="Scripts/jquery.zclip.min.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $("#copy-button").zclip({
            path: "http://codescratcher.com/download/ZeroClipboard.swf",
            copy: function () { return $('#txtCopyText').val(); },
            beforeCopy: function () { },
            afterCopy: function () {
                //alert('Copy To Clipboard : \n' + $('#txtCopyText').val());
            }
        });
    });
</script>
    </head>
<form id="form1" runat="server">
    <h2>
        Copy to clipboard
    </h2>
    <div style="width: 400px;">
        <h3>
            Text to Copy:
        </h3>
        <div style="clear: both; height: 10px;">
        </div>
        <div style="float: left; width: 100%;">
            <textarea rows="4" cols="50" ID="txtCopyText" TextMode="MultiLine" CssClass="txtStyle"  value="enter"> </textarea>
                
           
        </div>
        <div style="clear: both; height: 10px;">
        </div>
        <div style="text-align: center; float: right; width: 45%;">
            <input type="button" id="copy-button" class="copybtn" value="enter">
        </div>
    </div>
    </form>