<?php 
if ($_SERVER['SERVER_NAME'] == 'www.admissionangel.com') {
	$var= $_SERVER['REQUEST_URI'];
	$first = explode('/', $var);
	$dir=$first[1];
	if($dir=='testing'){
		$url	= 'http://www.admissionangel.com/testing/scrapper/test.php';
		$base_url = 'http://www.admissionangel.com/testing/admin/';
	}
	else{
		$url	= 'http://www.admissionangel.com/scrapper/test.php';
		$base_url = 'http://www.admissionangel.com/admin/';	
	}
}
else{
	$url	= 'http://localhost/scrapper/test.php';
	$base_url = 'http://127.0.0.1/admin/';
}
?>
<!DOCTYPE html>
<html lang="en" id ="all_page_holder">
	<head>
		<meta charset="UTF-8">
		<link rel="stylesheet" href="bootstrap.min.css" />
		
		<link rel="stylesheet" href="simple-sidebar.css" />
	    <link rel="stylesheet" href="custom_css.css" />
	    <title>Admission Angel Admin Panel</title>
	    <style>
			#input-form {
				width: 100%;
				height: auto;
				background-color: white;
				margin-left: %;
				margin-top: 0%;
				float: left;
			
			}
			
			#list {
				width: 100%
			}
			#container {
			}
		</style>
	</head>
	<body id="body_tag">
		<div class="container-fluid">
			<div class="row">
				
<nav class="navbar navbar-inverse navbar-static-top" id="top_navigation" style="margin:0px;">
	<div class="container-fluid">
	<!-- Brand and toggle get grouped for better mobile display -->
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand"  href="<?php echo $base_url."university/read"; ?>">Admission Angel</a>
			<a class="navbar-brand" style="margin-left: 30px" href="<?php echo $base_url."university/read"; ?>"><h5> Home </h5></a>
			<a class="navbar-brand" style="margin-left: 30px" href="<?php echo $base_url."analytics"; ?>"><h5> Analytics</h5></a>
			<!-- <a class="navbar-brand" style="margin-left: 20px" href="<?php echo $base_url."university/read"; ?>">Universities</a> -->
			<a class="navbar-brand" style="margin-left: 20px" href="<?php echo $base_url."users/users_list"; ?>"><h5>Users</h5></a>
			<a class="navbar-brand" style="margin-left: 20px" href="<?php echo $url;?>"><h5>Scraping Unit</h5></a>
			<a class="navbar-brand" style="margin-left: 20px" href="<?php echo $base_url."scraper/scraped_programs_list"; ?>"><h5>Scraped Programs</h5></a>
			<a class="navbar-brand" style="margin-left: 0px" href="<?php echo $base_url."university/all_programs_summary"; ?>"><h5>All Programs</h5></a>
			<a class="navbar-brand" style="margin-left: 0px" href="<?php echo $base_url."university/admin_logs"; ?>" ><h5>Logs</h5></a>
			<a class="navbar-brand" style="margin-left: 0px" href="<?php echo $base_url."login/signup_view"; ?>"><h5>SignUp</h5></a>
			<a class="navbar-brand" style="margin-left: 100px" id="btn-fblogin" href="<?php  echo $base_url."login/logout" ?>" ><h5>Logout</h5></a>
		
		</div>
		<!-- Collect the nav links, forms, and other content for toggling -->
		<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
		</div><!-- /.navbar-collapse -->
	</div><!-- /.container-fluid -->
</nav>